#!/bin/bash


#
# wrapper around w3m and some other tools designed
# to open Internet resources in terminal
#
# depends on w3m, reddio, zathura, transmission,
# vim, mpv (optionally feh)
#

shopt -s extglob

# Set options
# prefer GUI over TUI
[ -z $PREFER_GUI ] && PREFER_GUI=1
# prefer ENV over LNCH
[ -z $PREFER_ENV ] && PREFER_ENV=1
# prefer the current terminal over ENV
[ -z $PREFER_CUR ] && PREFER_CUR=0

# Check dependencies
# w3m
if ! hash w3m 2>/dev/null
then
    echo "ERROR: w3m is not installed; exiting"
    exit 1
fi
# mpv
if [ -z "$MPVAPP" ] || ! hash "${MPVAPP%% *}" 2>/dev/null
then
    if hash mpv 2>/dev/null
    then
        MPVAPP=$(which mpv)
    else
        echo "ERROR: mpv is not installed; exiting"
        exit 1
    fi
fi
# feh
if hash feh 2>/dev/null
then
    PICAPP="feh -q -F"
else
    PICAPP="$MPVAPP --really-quiet --input-conf=$HOME/.config/mpv/input.img.conf --geometry=100%x100% --image-display-duration=inf"
fi
# reddio
if ! hash reddio.sh 2>/dev/null && ! hash reddio 2>/dev/null
then
    echo "ERROR: reddio is not installed; exiting"
    exit 1
fi

# Detect environment
if command -v lnch > /dev/null 2>&1
then
    LNCH=( lnch )
elif command -v detach > /dev/null 2>&1
then
    LNCH=( detach )
fi

if ! [ -z $TMUX ]
then
    ENV=( tmux new-window )
else
    case $OSTYPE in
        darwin*)
            if ! [ -z $KITTY_PID ]
            then
                ENV=( /Applications/Kitty.app/Contents/MacOS/kitty sh -c )
            elif ! [ -z $ALACRITTY_WINDOW_ID ]
            then
                ENV=( /Applications/Alacritty.app/Contents/MacOS/alacritty -e sh -c )
            else
                ENV=( open -b com.apple.terminal )
            fi
            ;;
        linux*)
            if ! [ -z $KITTY_PID ]
            then
                ENV=( kitty sh -c )
                # ENV='kitty --start-as=maximized -o background=#2D313C -o foreground=#ababab -o font_size=14.0 sh -c'
            else
                ENV=( xterm -name tmp-xterm -title tmp-xterm -e )
            fi
            ;;
    esac
fi

[ ${#LNCH[@]} -eq 0 ] && LNCH=( "${ENV[@]}" )
[ ${PREFER_CUR} -eq 1 ] && ENV=( sh -c )

# Main
# if an actual filename was provided instead of an URL
# open it and exit
if [ -f $1 ]
then
    open $1
    exit
fi
# otherwise, proceed
case $1 in
    file:*|http*|\"http*)
        case $(echo $1 | awk '{ print tolower($0) }') in
            # documents
            *pdf)
                "${LNCH[@]}" sh -c "curl -sL \"$1\" | zathura --fork --mode fullscreen -"
                ;;

            # # commits
            # *github.com*commit*|*gitlab.com*commit*|*bitbucket.org*commit*|*git.sr.ht*commit*)
            #     "${ENV[@]}" "bash -ilc \"git-last-commits 25 $1 | LESSKEY=$HOME/.lesskey less -+F -R\""
            #     ;;
            # # patches
            # *lists.sr.ht*/patches/*)
            #     case "${ENV[@]}" in
            #         tmux*)
            #             ENV[${#ENV[@]}]="-n"
            #             ENV[$((${#ENV[@]}))+1]="Patch: $1"
            #             ;;
            #     esac
            #     "${ENV[@]}" "curl -sL https://lists.sr.ht$(curl -s $1 | grep -o '[^\"]*\/raw') | vim +'setf diff' +'nnoremap q ZZ' - --not-a-terminal"
            #     ;;

            # audio+video
            *v.redd.it*)
                [ ${PREFER_ENV} -eq 1 ] && LNCH=( "${ENV[@]}" )
                case "${LNCH[@]}" in
                    *lnch*|*detach*)
                        "${LNCH[@]}" $MPVAPP --really-quiet "$1/DASHPlaylist.mpd"
                        ;;
                    *)
                        "${LNCH[@]}" $MPVAPP "$1/DASHPlaylist.mpd"
                        ;;
                esac
                ;;
            *reddit.com*player)
                U=${1/reddit.com/v.redd.it}
                U=${U/\/www./\/}
                U=${U/\/video\//\/asset\/}
                U=${U/player/DASHPlaylist.mpd}
                [ ${PREFER_ENV} -eq 1 ] && LNCH=( "${ENV[@]}" )
                case "${LNCH[@]}" in
                    *lnch*|*detach*)
                        "${LNCH[@]}" $MPVAPP --really-quiet "$U"
                        ;;
                    *)
                        "${LNCH[@]}" $MPVAPP "$U"
                        ;;
                esac
                ;;
            *.m3u|*.m3u8|*.pls|*.aac|*.flac|*.mp3|*.mp4|*.mp4?*|*.m4a|*.m4b|*.m4v|*.mkv|*.mkv?*|*.mov|*.mov?*|*.ogv|*youtube.com*|*youtu.be*|*streamable.com*)
                [ ${PREFER_ENV} -eq 1 ] && LNCH=( "${ENV[@]}" )
                case "${LNCH[@]}" in
                    *lnch*|*detach*)
                        "${LNCH[@]}" $MPVAPP --really-quiet "$1"
                        ;;
                    *)
                        "${LNCH[@]}" $MPVAPP "$1"
                        ;;
                esac
                ;;
            # images
            *wikipedia.org*jpg|*wikipedia.org*png|*wikipedia.org*gif)
                U=$(curl -s "$1" | awk '/fullImageLink/{u = gensub(/.*"(\/\/upload[^"]+)".*/, "\\1", 1); print "https:"u}')
                case $U in
                    *gif)
                        $0 "$U"
                        ;;
                    *)
                        curl -sL "$U" | $PICAPP -
                        ;;
                esac
                ;;
            *.gif|*.gif?*|*.gifv|*.webp|*.webp?*)
                [ ${PREFER_ENV} -eq 1 ] && LNCH=( "${ENV[@]}" )
                case "${LNCH[@]}" in
                    *lnch*|*detach*)
                        "${LNCH[@]}" $MPVAPP --really-quiet --input-conf=$HOME/.config/mpv/input.img.conf --geometry=100%x100% --image-display-duration=inf "$1"
                        ;;
                    *)
                        "${LNCH[@]}" $MPVAPP --input-conf=$HOME/.config/mpv/input.img.conf --geometry=100%x100% --image-display-duration=inf "$1"
                        ;;
                esac
                ;;
            *reddit.com/gallery/*)
                IFS=$'\n' read -r -d '' -a gallery <<<$(curl -sL --user-agent 'Mozilla' "${1/gallery/comments}.json" | jq -r 'try .[].data.children[].data.media_metadata[].s.u, try .[].data.children[].data.media_metadata[].s.gif, try .[].data.children[].data.subreddit' | sed 's/&amp;/\&/g')
                if [ ${#gallery[@]} -gt 0 ]
                then
                    case "${gallery[-1]}" in
                        unixporn)
                            # limit unixporn galleries to just 2 first images
                            gallery=(${gallery[@]:0:2})
                            # # use an alternative image viewer for r/unixporn
                            # PICAPP="$MPVAPP --really-quiet --input-conf=$HOME/.config/mpv/input.img.conf --geometry=100%x100% --image-display-duration=inf"
                            ;;
                    esac
                else
                    echo "ERROR: No image links found"
                fi
                for gal in ${gallery[@]}
                do
                    case "$gal" in
                        http*)
                            curl -sL "$gal" | $PICAPP -
                            # # stop viewing if a non-zero exit code is received
                            # ! [ $? -eq 0 ] && break
                            ;;
                    esac
                done
                ;;
            *imgur.com/a/*|*imgur.com/gallery/*)
                IFS=$'\n' read -r -d '' -a gallery < <(curl -sL --location -g --request GET https://api.imgur.com/3/album/$(echo "$1" | awk -F/ '{if ($NF=="") {print $(NF-1)} else {print $NF}}') --header 'Authorization: Client-ID 921dde7df774ed2' | jq -r '.data.images[].link')
                for gal in ${gallery[@]}
                do
                    curl -sL "$gal" | $PICAPP -
                done
                ;;
            *imgur.com/*)
                if curl -sLI "$1" | grep -i content-type | grep -q text
                then
                    curl -sL "${1/imgur/i.imgur}.jpg" | $PICAPP -
                else
                    curl -sL "$1" | $PICAPP -
                fi
                ;;
            *pasteboard.co/*)
                curl -sL "$1" | awk '/img src/{print gensub(/.*(http[^"]*).*/, "\\1", 1)}' | xargs curl -s | $PICAPP -
                ;;
            *.jpg|*.jpg?*|*/jpeg|*@jpeg|*.jpeg|*.jpeg?*|*.png|*.png?*|\
                *preview.redd.it*|\
                *googleusercontent.com*|\
                *www.ft.com*.jpg?*|*www.ft.com*.png?*|*www.ft.com*.gif?*|*www.ft.com*images/raw/*)
                curl -sL "$1" | $PICAPP -
                ;;
            # reddit
            *reddit.com*)
                case $1 in
                    */comments/*|*/s/*|*/user/*)
                        case "${ENV[@]}" in
                            tmux*)
                                ENV[${#ENV[@]}]="-n"
                                ENV[$((${#ENV[@]}))+1]="NEWS/Reddio: $1"
                                ;;
                        esac
                        "${ENV[@]}" "reddio.sh \"$1\" | LESSKEY=$HOME/.lesskey less -+F -R"
                        ;;
                    *)
                        command -v openurl > /dev/null 2>&1 && openurl "$1" || open "$1"
                        ;;
                esac
                ;;
            https://redd.it/*)
                "${ENV[@]}" "reddio.sh \"${1/redd.it/www.reddit.com\/comments}\" | LESSKEY=$HOME/.lesskey less -+F -R"
                ;;
            # some sites to be openned with w3m/cgi-bin/_.cgi script
            http?(s)://?(www.)archive.org/details*|http?(s)://?(www.)archive.org/download*|\
                http?(s)://?(www.)seekingalpha.com/*|http?(s)://?(www.)ft.com/*|http?(s)://?(www.)cryptopotato.com*|http?(s)://?(www.)benzinga.com*|\
                http?(s)://?(www.)aljazeera.com/*|\
                http?(s)://?(www.)thecyberwire.com*|http?(s)://?(www.)understandingwar.org*|\
                http?(s)://?(www.)lrt.lt/*|http?(s)://?(www.)madeinvilnius.lt*|\
                http?(s)://?(www.)feedproxy.google.com/*|http?(s)://?(*.)blogspot.com/*|http?(s)://?(*.)wordpress.com/*|\
                http?(s)://?(www.)tokyotimes.org/*|http?(s)://?(www.)kootvela.com*|http?(s)://?(www.)travel.jurate.net/*|\
                http?(s)://?(www.)buvauten.lt/*|http?(s)://?(www.)nenamisedos.lt/*)
                URL=$1
                URL=${URL/:\/\//:\/\/___.}
                case "${ENV[@]}" in
                    tmux*)
                        ENV[${#ENV[@]}]="-n"
                        ENV[$((${#ENV[@]}))+1]="w3m: ${URL/___.}"
                        ;;
                esac
                "${ENV[@]}" "TERM=xterm-256color w3m -o confirm_qq=false \"$URL\""
                ;;
            # some sites to be openned with w3m/cgi-bin/=.cgi script
            *flibusta.*/b/*/read)
                URL=$1
                case "${ENV[@]}" in
                    tmux*)
                        ENV[${#ENV[@]}]="-n"
                        ENV[$((${#ENV[@]}))+1]="w3m: ${URL}"
                        ;;
                esac
                "${ENV[@]}" "TERM=xterm-256color w3m -o confirm_qq=false \"file:///cgi-bin/=.cgi?$URL\""
                ;;
            # other sites to be openned in w3m or in a GUI browser
            *)
                case $PREFER_GUI in
                    1)
                        command -v openurl > /dev/null 2>&1 && "${LNCH[@]}" openurl "$1" || "${LNCH[@]}" open "$1"
                        ;;
                    *)
                        case "${ENV[@]}" in
                            tmux*)
                                ENV[${#ENV[@]}]="-n"
                                ENV[$((${#ENV[@]}))+1]="w3m: $1"
                                ;;
                        esac
                        "${ENV[@]}" "TERM=xterm-256color w3m -o confirm_qq=false \"$1\""
                        ;;
                esac
                ;;
        esac
        ;;
    gopher?(s)://*|gemini://*)
        case "${ENV[@]}" in
            tmux*)
                ENV[${#ENV[@]}]="-n"
                ENV[$((${#ENV[@]}))+1]="w3m: $1"
                ;;
        esac
        "${ENV[@]}" "TERM=xterm-256color w3m -o confirm_qq=false \"$1\""
        ;;
    irc?(s)://*)
        "${ENV[@]}" "weechat-wrapper \"$1\" vpn"
        ;;
    nntp?(s)://*)
        # needs to include an actual newsgroup in the URL, e.g.: nntp://news.tin.org/tin.announce
        case "${ENV[@]}" in
            tmux*)
                ENV[${#ENV[@]}]="-n"
                ENV[$((${#ENV[@]}))+1]="w3m: $1"
                ;;
        esac
        "${ENV[@]}" "TERM=xterm-256color w3m -o confirm_qq=false -m \"$1\""
        ;;
    mailto:*)
        case "${ENV[@]}" in
            tmux*)
                ENV[${#ENV[@]}]="-S"
                ENV[${#ENV[@]}]="-n"
                ENV[$((${#ENV[@]}))+1]="Mail"
                ;;
        esac
        "${ENV[@]}" "TERM=xterm-256color aerc \"$1\""
        ;;
    magnet:*)
        if hash transmission-remote 2>/dev/null
        then
            "${ENV[@]}" "sudo -u $USER bash -ilc \"cd && tra a '$1'\""
        else
            echo "ERROR: transmission is not installed; exiting"
        fi
        ;;
    *)
        case "${ENV[@]}" in
            tmux*)
                ENV[${#ENV[@]}]="-n"
                ENV[$((${#ENV[@]}))+1]="w3m: $*"
                ;;
        esac
        if grep -q "^${1%%:*}:" ~/.w3m/urimethodmap
        then
            printf -v PAR %s%%20 "$@"
            "${ENV[@]}" "TERM=xterm-256color w3m -o confirm_qq=false \"${PAR%\%*}\""
        else
            "${ENV[@]}" "TERM=xterm-256color w3m -o confirm_qq=false \"https://lite.duckduckgo.com/lite/?kd=-1&q=$*\""
        fi
        ;;
esac
