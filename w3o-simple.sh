#!/bin/bash

#
# depends on zathura, vim, transmission, mpv (optionally feh)
#

# Options

# mpv
if [ -z "$MPVAPP" ] || ! hash "${MPVAPP%% *}" 2>/dev/null
then
    if hash mpv 2>/dev/null
    then
        MPVAPP=$(which mpv)
    else
        notify-send -i error -a "ERROR" "mpv is not installed"
        exit 1
    fi
fi

# feh
if hash feh 2>/dev/null
then
    PICAPP="feh -q -F"
else
    PICAPP="$MPVAPP --really-quiet --input-conf=$HOME/.config/mpv/input.img.conf --geometry=100%x100% --image-display-duration=inf"
fi

# Main

cd $HOME/Desktop

case ${1,,} in
    # www
    http*)
        case ${1,,} in
            # documents
            *pdf)
                setsid curl -sL "$1" | zathura --fork --mode fullscreen - || notify-send -i error -a "ERROR" "Failed to open" &
                ;;
            # audio+video
            *reddit.com*player)
                U=${1/reddit.com/v.redd.it}
                U=${U/\/www./\/}
                U=${U/\/video\//\/asset\/}
                U=${U/player/DASHPlaylist.mpd}
                setsid $MPVAPP --really-quiet "$U" || notify-send -i error -a "ERROR" "Failed to open" &
                ;;
            *v.redd.it*)
                setsid $MPVAPP --really-quiet "$1/DASHPlaylist.mpd" || notify-send -i error -a "ERROR" "Failed to open" &
                ;;
            *.m3u|*.m3u8|*.pls|*.aac|*.flac|*.mp3|*.mp4|*.mp4?*|*.m4a|*.m4b|*.m4v|*.mkv|*.mkv?*|*.mov|*.mov?*|*.ogv|*youtube.com*|*youtu.be*|*streamable.com*)
                setsid $MPVAPP --really-quiet "$1" || notify-send -i error -a "ERROR" "Failed to open" &
                ;;
            # images
            *wikipedia.org*jpg|*wikipedia.org*png|*wikipedia.org*gif)
                U=$(curl -s "$1" | awk '/fullImageLink/{u = gensub(/.*"(\/\/upload[^"]+)".*/, "\\1", 1); print "https:"u}')
                case $U in
                    *gif)
                        $0 "$U"
                        ;;
                    *)
                        curl -sL "$U" | $PICAPP -
                        ;;
                esac
                ;;
            *.gif|*.gif?*|*.gifv|*.webp|*.webp?*)
                setsid $MPVAPP --really-quiet --input-conf=$HOME/.config/mpv/input.img.conf --geometry=100%x100% --image-display-duration=inf "$1" || notify-send -i error -a "ERROR" "Failed to open" &
                ;;
            *reddit.com/gallery/*)
                IFS=$'\n' read -r -d '' -a gallery <<<$(curl -sL --user-agent 'Mozilla' "${1/gallery/comments}.json" | jq -r 'try .[].data.children[].data.media_metadata[].s.u, try .[].data.children[].data.media_metadata[].s.gif, try .[].data.children[].data.subreddit' | sed 's/&amp;/\&/g')
                if [ ${#gallery[@]} -gt 0 ]
                then
                    case "${gallery[-1]}" in
                        unixporn)
                            # limit unixporn galleries to just 2 first images
                            gallery=(${gallery[@]:0:2})
                            ;;
                    esac
                else
                    notify-send -i error -a "ERROR" "Failed to open"
                fi
                for gal in ${gallery[@]}
                do
                    case "$gal" in
                        http*)
                            setsid curl -sL "$gal" | $PICAPP - || notify-send -i error -a "ERROR" "Failed to open" &
                            ;;
                    esac
                done
                ;;
            *imgur.com/a/*|*imgur.com/gallery/*)
                IFS=$'\n' read -r -d '' -a gallery < <(curl -sL --location -g --request GET https://api.imgur.com/3/album/$(echo "$1" | awk -F/ '{if ($NF=="") {print $(NF-1)} else {print $NF}}') --header 'Authorization: Client-ID 921dde7df774ed2' | jq -r '.data.images[].link')
                for gal in ${gallery[@]}
                do
                    setsid curl -sL "$gal" | $PICAPP - || notify-send -i error -a "ERROR" "Failed to open" &
                done
                ;;
            *imgur.com*)
                if curl -sLI "$1" | grep -i content-type | grep -q text
                then
                    setsid curl -sL "${1/imgur/i.imgur}.jpg" | $PICAPP - || notify-send -i error -a "ERROR" "Failed to open" &
                else
                    setsid curl -sL "$1" | $PICAPP - || notify-send -i error -a "ERROR" "Failed to open" &
                fi
                ;;
            *pasteboard.co/*)
                curl -sL "$1" | awk '/img src/{print gensub(/.*(http[^"]*).*/, "\\1", 1)}' | xargs curl -s | $PICAPP -
                ;;
            *.jpg|*.jpg?*|*/jpeg|*@jpeg|*.jpeg|*.jpeg?*|*.png|*.png?*|\
                *preview.redd.it*|\
                *googleusercontent.com*|\
                *www.ft.com*.jpg?*|*www.ft.com*.png?*|*www.ft.com*.gif?*|*www.ft.com*images/raw/*)
                setsid curl -sL "$1" | $PICAPP - || notify-send -i error -a "ERROR" "Failed to open" &
                ;;
            # any other www resource
            *)
                openurl "$1"
                ;;
        esac
        ;;
    # everything else
    *)
        w3o "$1"
        ;;
esac
